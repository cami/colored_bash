# Colored Bash

- English: [About](#about-the-project)
- Deutsch: [Über](#Über)

## About the project

This repository just outputs some colors in bash.

You will see different colors and so you can choose the wanted color.

Below you'll see two screenshots from the generated table:

![](screenshots/whitetheme.png)

![](screenshots/darkTheme.png)

## License
GNU AGPLv3

## Über

Das Skript dieses Repositories gibt einfach die Farben in der Bash aus.

Man sieht die unterschiedlichen Farben und es kann danach gewählt werden, welche Farbe erwünscht ist.

Nachfolgend findest du zwei Screenshots der generierten Tabelle

![](screenshots/whitetheme.png)

![](screenshots/darkTheme.png)

## Lizenz
GNU AGPLv3
