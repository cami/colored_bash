#!/bin/bash

# Get the color definitions defined in a separate file
source promptcolors

# Create arrays with the different color definitions to iterate over them
readonly FOREGROUND_COLORS=( \
    "${F_BLACK}" \
    "${F_RED}" \
    "${F_GREEN}" \
    "${F_YELLOW}" \
    "${F_BLUE}" \
    "${F_MAGENTA}" \
    "${F_CYAN}" \
    "${F_WHITE}" \
)
readonly BACKGROUND_COLORS=( \
    "${B_BLACK}" \
    "${B_RED}" \
    "${B_GREEN}" \
    "${B_YELLOW}" \
    "${B_BLUE}" \
    "${B_MAGENTA}" \
    "${B_CYAN}" \
    "${B_WHITE}" \
)


# Function to print the colored things out
function printColored() {
    printf "$2$3%7s${COL_RESET}" "$1"
}

# Iterates over all color combinations
function iterateColors() {
    for bg_col in "${BACKGROUND_COLORS[@]}"; do
        for fg_col in "${FOREGROUND_COLORS[@]}"; do
            printColored "Test" "${fg_col}" "${bg_col}"
        done
        echo
    done
}

echo "You'll see different color combinations. Choose what you want :-)"
iterateColors
